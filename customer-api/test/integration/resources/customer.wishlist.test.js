const supertest = require('supertest');
const { app, ensureDatabaseConnection } = require('infrastructure');

const CustomerRepository =
  require('resources/customer/repositories/customerRepository');

const request = supertest(app);

describe('Customer Wishlist', () => {
  beforeAll(async () => {
    await ensureDatabaseConnection();4
  });

  describe('POST /customer/:id/wishlist', () => {
    beforeEach(async () => CustomerRepository.cleanup());

    test ('should return 400 status code for payload missing fields', async () => {
      const response = await request
        .post('/customer/xxxxxxxxxxxxxxxxxxxxxxxx/wishlist')
        .send({  });

      expect(response.status).toBe(400);
    });
  
    test ('should return 204 status code for a valid payload', async () => {
      let response = await request
        .post('/customer')
        .send({
          name: 'User Test',
          email: 'user@test.com',
        });

      const customer = response.body;

      expect(response.status).toBe(201);
      expect(customer).toHaveProperty('id');

      response = await request
        .post(`/customer/${customer.id}/wishlist`)
        .send({
          productId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
        });
    });
  });

  describe('GET /customer/:customerId?fields=wishlist', () => {
    beforeEach(async () => CustomerRepository.cleanup());

    let customerId;

    test ('should return 400 status code for invalid id', async () => {
      const response = await request
        .get('/customer/invalid-id')
        .send();

      expect(response.status).toBe(400);
    });

    test ('should return 200 status code for a valid request', async () => {
      let response = await request
        .post('/customer')
        .send({
          name: 'User Test',
          email: 'user@test.com',
        });

      customerId = response.body.id;

      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('id');

      response = await request
        .post(`/customer/${customerId}/wishlist`)
        .send({ productId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' });

      expect(response.status).toBe(204);

      response = await request
        .get(`/customer/${customerId}?fields=wishlist`)
        .send();

      expect(response.body).toHaveProperty('name');
      expect(response.body).toHaveProperty('email');
      expect(response.body).toHaveProperty('wishlist');
      expect(response.body.name).toBe('User Test');
      expect(response.body.email).toBe('user@test.com');
      expect(response.body.wishlist.length).toBe(1);
      expect(response.body.wishlist[0]).toBe('xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx');
    });
  });

  describe('DELETE /customer/:id/wishlist', () => {
    beforeEach(async () => CustomerRepository.cleanup());

    test ('should return 400 status code for payload missing fields', async () => {
      const response = await request
        .delete('/customer/xxxxxxxxxxxxxxxxxxxxxxxx/wishlist')
        .send({  });

      expect(response.status).toBe(400);
    });
  
    test ('should return 204 status code for a valid payload', async () => {
      let response = await request
        .post('/customer')
        .send({
          name: 'User Test',
          email: 'user@test.com',
        });

      customerId = response.body.id;

      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('id');

      response = await request
        .post(`/customer/${customerId}/wishlist`)
        .send({ productId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' });

      expect(response.status).toBe(204);

      response = await request
        .get(`/customer/${customerId}?fields=wishlist`)
        .send();

      expect(response.body).toHaveProperty('name');
      expect(response.body).toHaveProperty('email');
      expect(response.body).toHaveProperty('wishlist');
      expect(response.body.name).toBe('User Test');
      expect(response.body.email).toBe('user@test.com');
      expect(response.body.wishlist.length).toBe(1);
      expect(response.body.wishlist[0]).toBe('xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx');

      response = await request
        .delete(`/customer/${customerId}/wishlist`)
        .send({ productId: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx' });

      expect(response.status).toBe(204);

      response = await request
        .get(`/customer/${customerId}?fields=wishlist`)
        .send();

      expect(response.body).toHaveProperty('name');
      expect(response.body).toHaveProperty('email');
      expect(response.body).toHaveProperty('wishlist');
      expect(response.body.name).toBe('User Test');
      expect(response.body.email).toBe('user@test.com');
      expect(response.body.wishlist.length).toBe(0);
    });
  });
});
