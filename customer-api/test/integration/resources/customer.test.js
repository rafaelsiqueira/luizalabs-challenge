const supertest = require('supertest');
const { app, ensureDatabaseConnection } = require('infrastructure');

const CustomerRepository =
  require('resources/customer/repositories/customerRepository');

const request = supertest(app);

describe('Customer', () => {
  beforeAll(async () => {
    await ensureDatabaseConnection();
  });

  describe('POST /customer', () => {
    beforeEach(async () => CustomerRepository.cleanup());

    test ('should return 400 status code for payload missing fields', async () => {
      const response = await request
        .post('/customer')
        .send({ name: 'User Test' });

      expect(response.status).toBe(400);
    });

    test ('should return 400 status code for payload with invalid e-mail', async () => {
      const response = await request
        .post('/customer')
        .send({ name: 'User Test', email: 'email' });

      expect(response.status).toBe(400);
    });

    test ('should return 201 status code for a valid payload', async () => {
      const response = await request
        .post('/customer')
        .send({
          name: 'User Test',
          email: 'user@test.com',
        });

      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('id');
    });

    test ('should return 409 status code for duplicated entries (e-mail)', async () => {
      const data = {
        name: 'User Test',
        email: 'user@test.com',
      };

      const create = async (data) => {
        return request
          .post('/customer')
          .send(data);
      };

      let response = await create(data);
      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('id');

      response = await create(data);
      expect(response.status).toBe(409);
    });
  });

  describe('GET /customer/:customerId', () => {
    beforeEach(async () => CustomerRepository.cleanup());

    let customerId;

    test ('should return 400 status code for invalid id', async () => {
      const response = await request
        .get('/customer/invalid-id')
        .send();

      expect(response.status).toBe(400);
    });

    test ('should return 404 status code for invalid id', async () => {
      const response = await request
        .get('/customer/5d7bd411980e30a5b04ca513')
        .send();

      expect(response.status).toBe(404);
    });

    test ('should return 200 status code for a valid request', async () => {
      let response = await request
        .post('/customer')
        .send({
          name: 'User Test',
          email: 'user@test.com',
        });

      customerId = response.body.id;

      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('id');

      response = await request
        .get(`/customer/${customerId}`)
        .send();

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('name');
      expect(response.body).toHaveProperty('email');
      expect(response.body.name).toBe('User Test');
      expect(response.body.email).toBe('user@test.com');
    });
  });

  describe('PUT /customer/:customerId', () => {
    beforeEach(async () => CustomerRepository.cleanup());

    let customerId;

    test ('should return 400 status code for payload missing fields', async () => {
      const response = await request
        .put('/customer/123456789012345678901234')
        .send({ name: 'User Test'});

      expect(response.status).toBe(400);
    });

    test ('should return 400 status code for invalid id', async () => {
      const response = await request
        .put('/customer/invalid-id')
        .send({ name: 'User Test', email: 'email@email.com' });

      expect(response.status).toBe(400);
    });

    test ('should return 400 status code for payload with invalid e-mail', async () => {
      const response = await request
        .post('/customer')
        .send({ name: 'User Test', email: 'email' });

      expect(response.status).toBe(400);
    });

    test ('should return 204 status code for a valid payload', async () => {
      let response = await request
        .post('/customer')
        .send({
          name: 'User Test',
          email: 'user@test.com',
        });

      customerId = response.body.id;

      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('id');

      response = await request
        .put(`/customer/${customerId}`)
        .send({
          name: 'User Test Updated',
          email: 'user@test.com',
        });

      expect(response.status).toBe(204);
    });

    test ('should return 409 status code for duplicated entries (e-mail)', async () => {
      const data1 = {
        name: 'User Test 1',
        email: 'user1@test.com',
      };

      const data2 = {
        name: 'User Test 2',
        email: 'user2@test.com',
      };

      const create = async (data) => {
        return request
          .post('/customer')
          .send(data);
      };

      const customer1 = await create(data1);

      expect(customer1.status).toBe(201);
      expect(customer1.body).toHaveProperty('id');

      const customer2 = await create(data2);

      expect(customer2.status).toBe(201);
      expect(customer2.body).toHaveProperty('id');

      const response = await request
        .put(`/customer/${customer1.body.id}`)
        .send(data2);

      expect(response.status).toBe(409);
    });
  });

  describe('PATCH /customer/:customerId', () => {
    beforeEach(async () => CustomerRepository.cleanup());

    test ('should return 400 status code for payload missing fields', async () => {
      const response = await request
        .patch('/customer/xxxxxxxxxxxxxxxxxxxxxxxx')
        .send({ });

      expect(response.status).toBe(400);
    });

    test ('should return 400 status code for invalid id', async () => {
      const response = await request
        .put('/customer/xxx')
        .send({ name: 'User Test' });

      expect(response.status).toBe(400);
    });

    test ('should return 400 status code for payload with invalid e-mail', async () => {
      const response = await request
        .patch('/customer/x')
        .send({ email: 'email' });

      expect(response.status).toBe(400);
    });

    test ('should return 204 status code for a valid payload', async () => {
      let response = await request
        .post('/customer')
        .send({
          name: 'User Test',
          email: 'user@test.com',
        });

      customerId = response.body.id;

      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('id');

      response = await request
        .patch(`/customer/${customerId}`)
        .send({
          name: 'User Test Updated via Patch'
        });

      expect(response.status).toBe(204);
    });

    test ('should return 409 status code for duplicated entries (e-mail)', async () => {
      const data1 = {
        name: 'User Test 1',
        email: 'user1@test.com',
      };

      const data2 = {
        name: 'User Test 2',
        email: 'user2@test.com',
      };

      const create = async (data) => {
        return request
          .post('/customer')
          .send(data);
      };

      const customer1 = await create(data1);

      expect(customer1.status).toBe(201);
      expect(customer1.body).toHaveProperty('id');

      const customer2 = await create(data2);

      expect(customer2.status).toBe(201);
      expect(customer2.body).toHaveProperty('id');

      const response = await request
        .patch(`/customer/${customer1.body.id}`)
        .send({ email: data2.email });

      expect(response.status).toBe(409);
    });
  });

  describe('DELETE /customer/:customerId', () => {
    beforeEach(async () => CustomerRepository.cleanup());

    test ('should return 400 status code for invalid id', async () => {
      const response = await request
        .put('/customer/xxx')
        .send({ name: 'User Test' });

      expect(response.status).toBe(400);
    });

    test ('should return 204 status code for a valid request', async () => {
      let response = await request
        .post('/customer')
        .send({
          name: 'User Test',
          email: 'user@test.com',
        });

      customerId = response.body.id;

      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('id');

      response = await request
        .delete(`/customer/${customerId}`)
        .send();

      expect(response.status).toBe(204);
    });
  });
});
