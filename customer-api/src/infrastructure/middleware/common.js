const bodyParser = require('body-parser');
const compression = require('compression');

const helmet = require('helmet');
const cors = require('cors');

module.exports = [
  compression(),
  bodyParser.json(),
  helmet(),
  cors({
    origin: true,
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    allowedHeader: ['Content-Type', 'Authorization'],
  }),
];
