const { MongoClient } = require('mongodb');
const { mongodb } = require('../config');
const AbstractRepository = require('./AbstractRepository');

module.exports = async () => {
  const client = new MongoClient(mongodb.url, {
    ...mongodb.options,
  });

  const connectedClient = await client.connect();
  const db = connectedClient.db(mongodb.databaseName);

  AbstractRepository.setDatabaseClient(db);
};
