module.exports = {
  up(db) {
    return db.collection('customers').createIndex({ email: 1 }, { name: 'idx-email-unique', unique: true });
  },

  down(db) {
    return db.collection('customers').dropIndex('idx-email-unique');
  },
};
