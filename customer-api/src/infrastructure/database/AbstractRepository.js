const { errorsHelper, DefaultError, DuplicateKeyError } = require('infrastructure/errors');

module.exports = class AbstractRepository {
  static setDatabaseClient(databaseClient = null) {
    if (databaseClient) {
      this.databaseClient = databaseClient;
    }

    return this.databaseClient;
  }

  static throwError(e) {
    switch (true) {
      case errorsHelper.isDuplicateKeyError(e):
        throw new DuplicateKeyError(e);
      default:
        throw new DefaultError(e);
    }
  }
};
