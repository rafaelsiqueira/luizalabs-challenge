module.exports = {
  appName: 'customer-api',
  logLevel: 'debug',
  server: {
    port: 3001,
  },

  mongodb: {
    url: process.env.MONGO_DB_URL || 'mongodb://localhost:27017',
    databaseName: process.env.MONGO_DB_NAME || 'luizalabs_challenge',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },

  migrationsDir: `${__dirname}/../database/migrations`,
  changelogCollectionName: 'changelog',
};
