const bunyan = require('bunyan');
const BunyanPrettyStream = require('bunyan-prettystream');

module.exports = (appName, logLevel) => {
  const prettyStream = new BunyanPrettyStream({ useColor: true });
  prettyStream.pipe(process.stdout);

  return bunyan.createLogger({
    name: appName,
    streams: [
      {
        type: 'raw',
        stream: prettyStream,
        level: logLevel,
      },
    ],
    serializers: {
      req: bunyan.stdSerializers.req,
      res: bunyan.stdSerializers.res,
      err: bunyan.stdSerializers.err,
    },
  });
};
