const app = require('./app');
const config = require('./config');
const logger = require('./logger')(config.appName, config.logLevel);
const ensureDatabaseConnection = require('./database');

module.exports = {
  app,
  config,
  logger,
  ensureDatabaseConnection,
};
