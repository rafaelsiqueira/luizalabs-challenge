const {
  app, logger, ensureDatabaseConnection, config,
} = require('infrastructure');

module.exports = async () => {
  await ensureDatabaseConnection();

  app.listen(config.server.port, () => logger.info(`Listening on port ${config.server.port}`));
};
