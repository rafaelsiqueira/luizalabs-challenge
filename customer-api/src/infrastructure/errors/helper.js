module.exports = {
  isDuplicateKeyError(e) {
    return e.message.includes('E11000');
  },
};
