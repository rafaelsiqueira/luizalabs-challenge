const DefaultError = require('./defaultError');

class DuplicateKeyError extends DefaultError {
  constructor(e) {
    super(e);
    this.statusCode = 409;
    this.message = this.message.substring(this.message.indexOf('index:') + 6);
  }
}

module.exports = DuplicateKeyError;
