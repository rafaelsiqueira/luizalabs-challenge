const { ObjectID } = require('mongodb');

class CustomerRepository {
  save(data) {
    return this.databaseClient.save(data);
  }

  remove(id) {
    return this.databaseClient.remove({ _id: ObjectID(id) });
  }
}

module.exports = CustomerRepository;
