module.exports = (repository) => async (req, res, next) => {
  try {
    const data = { ...req.body };

    await repository.create(data);

    res.status(201).send({ id: data._id });
    return next();
  } catch (e) {
    return next(e);
  }
};
