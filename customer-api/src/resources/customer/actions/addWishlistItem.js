module.exports = (repository) => async (req, res, next) => {
  try {
    const { id } = req.params;
    const { productId } = { ...req.body };

    await repository.addWishlistItem(id, productId);

    res.status(204).send();
    return next();
  } catch (e) {
    return next(e);
  }
};
