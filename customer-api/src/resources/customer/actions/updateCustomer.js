module.exports = (repository) => async (req, res, next) => {
  try {
    const { id } = req.params;
    const data = { ...req.body };

    delete data.id;
    delete data._id;

    await repository.update(id, data);

    res.status(204).send();
    return next();
  } catch (e) {
    return next(e);
  }
};
