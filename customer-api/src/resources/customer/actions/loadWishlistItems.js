module.exports = (repository) => async (req, res, next) => {
  try {
    const { id } = req.params;
    const { page } = req.query;

    const items = await repository.fetchWishlist(id, page || 1);

    res.status(200).send(items);
    return next();
  } catch (e) {
    return next(e);
  }
};
