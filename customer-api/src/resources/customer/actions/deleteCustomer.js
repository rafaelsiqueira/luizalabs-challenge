module.exports = (repository) => async (req, res, next) => {
  try {
    const { id } = req.params;

    await repository.remove(id);

    res.status(204).send();
    return next();
  } catch (e) {
    return next(e);
  }
};
