module.exports = (repository) => async (req, res, next) => {
  try {
    const { id } = req.params;
    const { fields, page } = req.query;

    const loads = [repository.findOne(id)];

    if (fields && fields.includes('wishlist')) {
      loads.push(repository.fetchWishlist(id, page || 1));
    }

    const [customer, wishlist] = await Promise.all(loads);

    if (!customer) {
      return res.status(404).send();
    }

    const data = { ...customer, ...wishlist };

    res.status(200).send(data);
    return next();
  } catch (e) {
    return next(e);
  }
};
