const create = require('./createCustomer');
const update = require('./updateCustomer');
const remove = require('./deleteCustomer');
const load = require('./loadCustomer');

const addItem = require('./addWishlistItem');
const removeItem = require('./removeWishlistItem');
const loadItems = require('./loadWishlistItems');

module.exports = {
  customer: {
    create, update, remove, load,
  },
  wishlist: {
    addItem, removeItem, loadItems,
  },
};
