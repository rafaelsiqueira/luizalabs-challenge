const { Router } = require('express');

const idSchema = require('./validation/ids');
const customerSchema = require('./validation/customer');
const customerPartialSchema = require('./validation/customer.partial');

const repository = require('./repositories/customerRepository');

const actions = require('./actions').customer;

module.exports = ((validate) => {
  const router = new Router();

  router
    .route('/customer')
    .post(validate({ body: customerSchema }), actions.create(repository));

  router
    .route('/customer/:id')
    .get(validate({ params: idSchema }), actions.load(repository))
    .put(validate({ params: idSchema, body: customerSchema }), actions.update(repository))
    .patch(validate({ params: idSchema, body: customerPartialSchema }), actions.update(repository))
    .delete(validate({ params: idSchema }), actions.remove(repository));

  return router;
});
