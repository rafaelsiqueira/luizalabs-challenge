const { Router } = require('express');

const idSchema = require('./validation/ids');
const bodySchema = require('./validation/wishlist');

const repository = require('./repositories/customerRepository');

const actions = require('./actions').wishlist;

module.exports = ((validate) => {
  const router = new Router();

  router
    .route('/customer/:id/wishlist')
    .get(validate({ params: idSchema }), actions.loadItems(repository))
    .post(validate({ params: idSchema, body: bodySchema }), actions.addItem(repository))
    .delete(validate({ params: idSchema, body: bodySchema }), actions.removeItem(repository));

  return router;
});
