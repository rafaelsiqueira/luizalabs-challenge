const customer = require('./customer');
const wishlist = require('./wishlist');

module.exports = (schemaValidate) => [
  customer(schemaValidate),
  wishlist(schemaValidate),
];
