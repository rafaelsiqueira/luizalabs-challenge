const { ObjectID } = require('mongodb');

const AbstractRepository = require('infrastructure/database/AbstractRepository');

const WISHLIST_NUMBER_OF_ITEMS = 5;

class CustomerRepository extends AbstractRepository {
  static async create(data) {
    try {
      return await this.databaseClient.collection('customers').insertOne(data);
    } catch (e) {
      this.throwError(e);
      return false;
    }
  }

  static async update(id, data) {
    try {
      return await this.databaseClient.collection('customers').updateOne({ _id: ObjectID(id) }, { $set: data });
    } catch (e) {
      this.throwError(e);
      return false;
    }
  }

  static async findOne(id) {
    try {
      return await this.databaseClient
        .collection('customers')
        .findOne({ _id: ObjectID(id) }, { fields: { _id: 0, name: 1, email: 1 } });
    } catch (e) {
      this.throwError(e);
      return false;
    }
  }

  static async fetchWishlist(id, page = 1) {
    try {
      const skip = WISHLIST_NUMBER_OF_ITEMS * (page - 1);

      const data = await this.databaseClient
        .collection('customers')
        .findOne({
          _id: ObjectID(id),
        }, {
          fields: {
            _id: 0,
            email: 0,
            name: 0,
            wishlist: {
              $slice: [skip, WISHLIST_NUMBER_OF_ITEMS],
            },
          },
        });

      return data;
    } catch (e) {
      this.throwError(e);
      return false;
    }
  }

  static async addWishlistItem(id, product) {
    try {
      return await this.databaseClient
        .collection('customers')
        .updateOne({ _id: ObjectID(id) }, { $addToSet: { wishlist: product } });
    } catch (e) {
      this.throwError(e);
      return false;
    }
  }

  static async removeWishlistItem(id, product) {
    try {
      return await this.databaseClient
        .collection('customers')
        .updateOne({ _id: ObjectID(id) }, { $pull: { wishlist: product } });
    } catch (e) {
      this.throwError(e);
      return false;
    }
  }

  static async remove(id) {
    try {
      return await this.databaseClient.collection('customers').deleteOne({ _id: ObjectID(id) });
    } catch (e) {
      this.throwError(e);
      return false;
    }
  }

  // just for testing
  static async cleanup() {
    return this.databaseClient.collection('customers').deleteMany({});
  }
}

module.exports = CustomerRepository;
