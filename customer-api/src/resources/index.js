const customer = require('./customer');

module.exports = (schemaValidate) => [
  customer(schemaValidate),
];
