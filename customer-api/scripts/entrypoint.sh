cd /var/app/

echo "$@"

if [ "$1" = 'migrate' ]; then
  exec yarn run migration:up
  exit 0
fi

if [ "$1" = 'test' ]; then
  exec yarn run start
  exit 0
fi

if [ "$1" = 'start' ]; then
  exec yarn run start
  exit 0
fi

exec "$@"