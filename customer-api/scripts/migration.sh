#!/usr/bin/env sh

command=$1
configFile=$(pwd)/src/infrastructure/config/index.js

shift 

npx migrate-mongo -f $configFile $command $@
