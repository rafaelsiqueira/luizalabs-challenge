# Luiza Labs - Challenge

## Stack Principal
- NodeJS (express, jwt, jest)
- MongoDB

## Estrutura

O desafio foi separado em dois projetos, Proxy/Gateway e Customer API. Abaixo algumas vantagens na abordagem do modelo Proxy/Gateway para exposição de APIs públicas:

- Governança
- Promover desacoplamento entre os diferentes serviços (Produtos e Cliente neste caso);
- Segurança/Autenticação
- Cache

Obs.: O Proxy/Gateway implementado serve apenas de referência para a arquitetura. Idealmente seria utilizado serviços como APIGee, AWS API Gateway, etc.

![Application Flow](https://bitbucket.org/rafaelsiqueira/luizalabs-challenge/raw/3ceced2e1ff5eaf1cf15cc5a21035dc5a72b3a0c/documentation/app-flow.png)

Os serviços estão estruturados da seguinte forma:

![Serviços](https://bitbucket.org/rafaelsiqueira/luizalabs-challenge/raw/3ceced2e1ff5eaf1cf15cc5a21035dc5a72b3a0c/documentation/microservice.png)

Onde:

#### Infrastructure
Está relacionada a estrutura base da aplicação como configurações, loggers, middlewares e utilitários;

#### Routes/Actions
São as rotas e actions disponíveis na API

#### Service/Repository
São respectivamente as camadas que encapsulam um comportamento/funcionalidade e acesso à dados

## Observação

A API do desafio parece não ter suporte a requisições simultâneas a partir de uma mesma origem.
Ao chamar simultâneamente o serviço retorna `503`.

Possivelmente existe algum throttle ou algo do tipo para restringir.

Do ponto de vista de performance poderia ser feita a projeção dos atributos do produto na base da Customer API no momento que é feito o registro como favorito. Essa abordagem traria uma _complexidade_ no caso de atualização desses atributos na base de produtos.

Para fins deste desafio utilizei uma abordagem mais simples, "serializando" as requisições, o que aumenta o tempo de resposta dependendo da quantidade de produtos favoritos que um cliente possui.

O código _paralelo_ encontra-se comentado em `/api-proxy-gateway/src/resources/customer/transformer/index.js:12`.

## Como rodar?

### Requisitos mínimos

- Utilizar Linux/Mac (deveria funcionar no windows também, mas não tenho um para testar aqui);
- Docker e Docker Compose instalados

### Passo a passo

- Clonar/Baixar o projeto
- Executar `docker-compose up`

### Autenticação

Antes de consumir os endpoints da Customer API é necessário se autenticar. Para tal, basta enviar um `POST` para a rota `/auth` utilizando o payload a seguir:

```
{
	"clientId": "luizalabs",
	"clientPwd": "ph8g8hvCHt"
}
```

Você receberá um token e deverá informá-lo nas requisições seguintes através do Header `Authorization: Bearer <TOKEN>`.

A documentação da API está disponível via swagger, no endereço http://localhost:3000/api-docs.

### Testes

O projeto Customer API possui alguns testes de integração, testando os principais casos. Não foram criados testes para o Proxy/Gateway por se tratar de uma espécie de "stub".

Para rodar os testes basta executar `docker-compose run --rm customer-api yarn test`

## Evoluções

- Autenticação - Seria ideal ter um data store para armazenamento/gestão dos tokens gerados
- Cobertura de testes
- Event Sourcing

