const ProductProxy = require('proxies/productProxy');
const CustomerProxy = require('proxies/customerProxy');

module.exports = {
  ProductProxy,
  CustomerProxy,
};
