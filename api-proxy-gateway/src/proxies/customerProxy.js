const { customer } = require('infrastructure/config').services;
const { logger } = require('infrastructure');
const proxy = require('express-http-proxy');
const fetch = require('node-fetch');
const { NotFoundError } = require('infrastructure/errors');

const customerProxy = proxy(customer);

const responseBody = async (response) => {
  let body = {};
  try {
    body = await response.json();
  } catch (e) {
    logger.error(e);
  }
  return body;
};

const handleResponseError = async (fetchResponse, httpResponse, next) => {
  switch (fetchResponse.status) {
    case 404:
      throw new NotFoundError();

    default:
      httpResponse.status(fetchResponse.status).send(await (responseBody(fetchResponse)));
      break;
  }

  return next();
};

module.exports = {
  forward() {
    return customerProxy;
  },

  async fetchWithTransformer(responseTransformer, req, res, next) {
    try {
      const customerResponse = await fetch(`${customer}${req.url}`);

      if (!customerResponse.ok) {
        return handleResponseError(customerResponse, res);
      }

      const data = await customerResponse.json();

      res.status(200).send(await (responseTransformer(data)));

      return next();
    } catch (e) {
      return next(e);
    }
  },
};
