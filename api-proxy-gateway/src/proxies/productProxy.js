const fetch = require('node-fetch');
const { product } = require('infrastructure/config').services;
const { NotFoundError } = require('infrastructure/errors');

const productDetail = async (productId) => {
  const response = await fetch(`${product}/${productId}`);

  if (response.status === 404) {
    throw new NotFoundError(`Product ${productId} not found.`);
  }

  return response.json();
};

const checkForProduct = async (req, res, next) => {
  try {
    const { productId } = req.body;

    await productDetail(productId);

    return next();
  } catch (e) {
    return next(e);
  }
};

module.exports = {
  checkForProduct,
  detail: productDetail,
};
