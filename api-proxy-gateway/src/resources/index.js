const auth = require('./auth');
const customer = require('./customer');

module.exports = (authenticationMiddlware, schemaValidate) => [
  auth(schemaValidate),
  customer(authenticationMiddlware),
];
