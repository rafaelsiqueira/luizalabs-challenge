const customer = require('./customer');
const wishlist = require('./wishlist');

module.exports = (authenticationMiddleware) => [
  customer(authenticationMiddleware),
  wishlist(authenticationMiddleware),
];
