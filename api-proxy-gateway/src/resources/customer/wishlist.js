const { Router } = require('express');
const { CustomerProxy, ProductProxy } = require('proxies');
const resultTransformer = require('./transformer');

module.exports = (authenticationMiddleware) => {
  const router = new Router();

  /**
   * @typedef Wishlist
   * @property {Array.<Product>} wishlist.required
   */

  /**
   * @typedef WishlistBody
   * @property {string} productId.required
   */

  router
    .route('/customer/:id/wishlist')

    /**
     * Lista produtos favoritos de um cliente
     *
     * @route GET /customer/{customerId}/wishlist
     * @group Favoritos
     * @param {string} customerId.path.required
     * @param {integer} page.query.required
     * @param {Wishlist.model} wishlist.body.required
     * @produces application/json
     * @returns 204
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .get(authenticationMiddleware, CustomerProxy.fetchWithTransformer.bind(null, resultTransformer))

    /**
     * Adicionar um produto favorito
     *
     * @route POST /customer/{customerId}/wishlist
     * @group Favoritos
     * @param {string} customerId.path.required
     * @param {WishlistBody.model} wishlist.body.required
     * @produces application/json
     * @returns 204
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .post(
      authenticationMiddleware,
      ProductProxy.checkForProduct,
      CustomerProxy.forward(),
    )

    /**
     * Remover um produto favorito
     *
     * @route DELETE /customer/{customerId}/wishlist
     * @group Favoritos
     * @param {string} customerId.path.required
     * @param {WishlistBody.model} wishlist.body.required
     * @produces application/json
     * @returns 204
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .delete(authenticationMiddleware, CustomerProxy.forward());

  return router;
};
