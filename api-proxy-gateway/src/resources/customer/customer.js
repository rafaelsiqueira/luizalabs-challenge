const { Router } = require('express');
const { CustomerProxy } = require('proxies');
const resultTransformer = require('./transformer');

module.exports = (authenticationMiddleware) => {
  const router = new Router();

  /**
   * @typedef CustomerId
   * @property {string} id.required
   */

  /**
   * @typedef Product
   * @property {string} id.required
   * @property {string} title.required
   * @property {string} brand.required
   * @property {string} image.required
   * @property {number} price.required
   * @property {number} reviewScore
   */

  /**
   * @typedef Customer
   * @property {string} name.required
   * @property {string} email.required
   */

  /**
   * @typedef CustomerPartial
   * @property {string} name
   * @property {string} email
   */

  /**
   * @typedef CustomerFull
   * @property {string} name.required
   * @property {string} email.required
   * @property {Array.<Product>} wishlist
   */

  router
    .route('/customer')

    /**
     * Criação de novo cliente
     *
     * @route POST /customer
     * @group Cliente
     * @param {Customer.model} customer.body.required
     * @produces application/json
     * @returns {CustomerId.model} 200
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .post(authenticationMiddleware, CustomerProxy.forward());

  router
    .route('/customer/:id')

    /**
     * Criação de novo cliente
     *
     * @route GET /customer/{id}
     * @group Cliente
     * @param {string} id.path.required
     * @param {string} fields.query - eg: wishlist
     * @produces application/json
     * @returns {CustomerFull.model} 200
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .get(authenticationMiddleware, CustomerProxy.fetchWithTransformer.bind(null, resultTransformer))

    /**
     * Atualização de um cliente
     *
     * @route PUT /customer/{id}
     * @group Cliente
     * @param {string} id.path.required
     * @param {Customer.model} customer.body.required
     * @produces application/json
     * @returns 204
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .put(authenticationMiddleware, CustomerProxy.forward())

    /**
     * Atualização parcial de um cliente
     *
     * @route PATCH /customer/{id}
     * @group Cliente
     * @param {string} id.path.required
     * @param {CustomerPartial.model} customer.body.required
     * @produces application/json
     * @returns 204
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .patch(authenticationMiddleware, CustomerProxy.forward())

    /**
     * Remoção de um cliente
     *
     * @route DELETE /customer/{id}
     * @group Cliente
     * @param {string} id.path.required
     * @produces application/json
     * @returns 204
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .delete(authenticationMiddleware, CustomerProxy.forward());

  return router;
};
