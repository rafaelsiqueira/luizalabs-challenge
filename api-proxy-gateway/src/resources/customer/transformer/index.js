const { ProductProxy } = require('proxies');
const { series } = require('async');

module.exports = async (data) => {
  if (!data.wishlist) {
    return data;
  }

  // A API do desafio parece não ter suporte a requisições simultâneas a partir de uma mesma origem.
  // Ao chamar simultâneamente, a partir de 2 requisições o serviço retorna 503.
  // Possivelmente existe algum throttle ou algo do tipo para restringir.
  //
  // const allProducts = await Promise.all(
  //   data.wishlist.map((p) => ProductProxy.detail(p)),
  // );

  const allProducts = await series(
    data.wishlist.map((p) => async () => ProductProxy.detail(p)),
  );

  return { ...data, wishlist: allProducts };
};
