const { Router } = require('express');
const { auth } = require('infrastructure/config');
const jwt = require('jsonwebtoken');
const authSchema = require('./validation/auth');

module.exports = ((schemaValidate) => {
  const router = new Router();

  router
    .route('/auth')

  /**
     * @typedef Auth
     * @property {string} token.required
     */

  /**
     * @typedef Client
     * @property {string} clientId.required
     * @property {string} clientPwd.required
     */

  /**
     * @typedef Unauthorized
     * @property {integer} statusCode.required
     * @property {string} message.required
     */

    /**
     * Rota para autenticação
     *
     * @route POST /auth
     * @group Autenticação
     * @param {Client.model} client.body.required
     * @returns {Auth.model} 200
     * @returns {Unauthorized.model}  401 - Unauthorized
     */
    .post(schemaValidate({ body: authSchema }), (req, res, next) => {
      try {
        const { clientId, clientPwd } = req.body;

        if (auth.clientId === clientId && auth.password === clientPwd) {
          const token = jwt.sign({ clientId },
            auth.secretKey,
            { expiresIn: '24h' });

          return res.status(200).send({
            token,
          });
        }

        return res.status(401).send({
          statusCode: 401,
          message: 'Invalid clientId or password',
        });
      } catch (e) {
        return next(e);
      }
    });

  return router;
});
