const app = require('./app');
const config = require('./config');
const logger = require('./logger')(config.appName, config.logLevel);
const swagger = require('./swagger');

module.exports = {
  app,
  config,
  logger,
  swagger,
};
