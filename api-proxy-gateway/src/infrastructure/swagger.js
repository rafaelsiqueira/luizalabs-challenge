const swaggerGenerator = require('express-swagger-generator');

module.exports = (app) => {
  const expressSwagger = swaggerGenerator(app);

  const options = {
    swaggerDefinition: {
      info: {
        description: 'API Docs',
        title: 'Luiza Labs - Challenge',
        version: '1.0.0',
      },
      host: 'localhost:3000',
      basePath: '/',
      produces: [
        'application/json',
        'application/xml',
      ],
      schemes: ['http', 'https'],
      securityDefinitions: {
        JWT: {
          type: 'apiKey',
          in: 'header',
          name: 'Authorization',
          description: '',
        },
      },
    },
    basedir: __dirname, // app absolute path
    files: ['../resources/**/**/*.js'], // Path to the API handle folder
  };

  expressSwagger(options);
};
