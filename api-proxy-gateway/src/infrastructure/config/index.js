module.exports = {
  appName: 'api-proxy-gateway',
  logLevel: 'debug',
  server: {
    port: 3000,
  },

  services: {
    customer: process.env.CUSTOMER_API_ENDPOINT || 'http://localhost:3001',
    product: process.env.PRODUCT_API_ENDPOINT || 'http://challenge-api.luizalabs.com/api/product',
  },

  auth: {
    secretKey: process.env.AUTHENTICATION_KEY,
    clientId: 'luizalabs',
    password: 'ph8g8hvCHt',
  },
};
