const {
  app, logger, config, swagger,
} = require('infrastructure');

module.exports = async () => {
  swagger(app);

  app.listen(config.server.port, () => logger.info(`Listening on port ${config.server.port}`));
};
