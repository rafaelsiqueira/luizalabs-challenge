class DefaultError extends Error {
  constructor(e) {
    super(e);
    this.statusCode = 500;
  }
}

module.exports = DefaultError;
