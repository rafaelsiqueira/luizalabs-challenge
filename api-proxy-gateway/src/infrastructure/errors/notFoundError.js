const DefaultError = require('./defaultError');

class NotFoundError extends DefaultError {
  constructor(e) {
    super(e);
    this.statusCode = 404;
  }
}

module.exports = NotFoundError;
