const DefaultError = require('./defaultError');
const NotFoundError = require('./notFoundError');

module.exports = {
  DefaultError,
  NotFoundError,
};
