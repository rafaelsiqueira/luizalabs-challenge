const common = require('./common');
const authentication = require('./authentication');

module.exports = {
  common,
  authentication,
};
