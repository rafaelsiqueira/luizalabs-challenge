const jwt = require('jsonwebtoken');
const { auth } = require('infrastructure/config');

module.exports = (req, res, next) => {
  let token = req.headers['x-access-token'] || req.headers.authorization;

  if (!token) {
    return res.status(401).send({
      message: 'No token provided',
    });
  }

  if (token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
  }

  jwt.verify(token, auth.secretKey, (err, user) => {
    if (err) {
      return res.status(401).send({
        message: 'Invalid Token',
      });
    }
    req.user = user;
    return next();
  });

  return true;
};
