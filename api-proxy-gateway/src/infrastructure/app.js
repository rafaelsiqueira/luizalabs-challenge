const Express = require('express');
const { common, authentication } = require('infrastructure/middleware');
const { Validator, ValidationError } = require('express-json-validator-middleware');

const resources = require('resources')(
  authentication,
  new Validator({ allErrors: true }).validate,
);

const app = new Express();

app.use(common);
app.use(resources);

app.use((err, req, res, next) => {
  if (err instanceof ValidationError) {
    res.status(400).send({ message: 'Bad Request', errors: err.validationErrors });
    return next();
  }

  const statusCode = err.statusCode || 500;

  res.status(statusCode).send({
    statusCode,
    message: err.message,
  });

  return true;
});

module.exports = app;
